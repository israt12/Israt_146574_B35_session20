<?php

namespace App;
class Person
{
    public $name="Israt";
    public $gender="Female";
    public $blood_group="O+";

    public function showPersonInfo()
    {
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }
}