<?php
use App\Person;                                          //connected with namespace
use App\Student;

/*
require_once("../../src/BITM/SEIP146574/Person.php");
require_once("../../src/BITM/SEIP146574/Student.php");
*/

function __autoload($className)                             //autoload is magic method
{
    echo $className."<br>";                                 // show the className of Person& Student
    list($ns,$cn)=explode("\\",$className);                 //ns=namespace,cn=classname
    require_once("../../src/BITM/SEIP146574/".$cn.".php");  //classname concatenate with  php
}

$objPerson=new Person();
echo $objPerson->showPersonInfo();


$objStudent=new Student();
echo $objStudent->showStudentInfo();